-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2021 at 11:01 PM
-- Server version: 8.0.18
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `get2gether3`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `nomblog` varchar(52) NOT NULL,
  `dateblog` datetime NOT NULL,
  `photo` varchar(52) NOT NULL,
  `pseudocreateur` varchar(52) NOT NULL,
  `statut` varchar(52) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `idchat` int(11) NOT NULL,
  `pseudoorigin` varchar(52) NOT NULL,
  `pseudodestinataire` varchar(52) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`idchat`, `pseudoorigin`, `pseudodestinataire`) VALUES
(1, 'Claudiu', 'Omar'),
(6, 'Claudiu', 'David'),
(7, 'Claudiu', 'Frank'),
(8, 'Claudiu', 'Dino'),
(9, 'Omar', 'Claudiu'),
(10, 'Omar', 'David'),
(11, 'Omar', 'Frank'),
(12, 'Omar', 'Dino');

-- --------------------------------------------------------

--
-- Table structure for table `commentaireblog`
--

CREATE TABLE `commentaireblog` (
  `idcommentaire` int(11) NOT NULL,
  `nomblog` varchar(52) NOT NULL,
  `pseudoorigin` varchar(52) NOT NULL,
  `datecommentaire` datetime NOT NULL,
  `commentaire` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `commentaireeventment`
--

CREATE TABLE `commentaireeventment` (
  `idevenement` int(11) NOT NULL,
  `nomevenement` varchar(52) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `pseudoorigin` varchar(52) NOT NULL,
  `datecommentaire` datetime NOT NULL,
  `commentaire` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comptemembre`
--

CREATE TABLE `comptemembre` (
  `idcompte` int(11) NOT NULL,
  `typemembre` varchar(52) NOT NULL,
  `pseudo` varchar(52) NOT NULL,
  `courriel` varchar(52) NOT NULL,
  `motdepasse` varchar(52) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `comptemembre`
--

INSERT INTO `comptemembre` (`idcompte`, `typemembre`, `pseudo`, `courriel`, `motdepasse`) VALUES
(3, 'Application', 'David', 'davidr@hotmail.com', 'davidr'),
(4, 'Application', 'Dino', 'dinoa@hotmail.com', 'dinoa'),
(5, 'Application', 'Claudiu', 'claudiut@hotmail.com', 'claudiut'),
(6, 'Application', 'Frank', 'frankb@hotmail.com', 'frankb'),
(7, 'Application', 'Omar', 'omara@hotmail.com', 'omara');

-- --------------------------------------------------------

--
-- Table structure for table `evenement`
--

CREATE TABLE `evenement` (
  `nomevenement` varchar(52) NOT NULL,
  `dateevenement` date NOT NULL,
  `heure` time NOT NULL,
  `lieu` varchar(52) NOT NULL,
  `description` varchar(120) NOT NULL,
  `pseudocreateur` varchar(52) NOT NULL,
  `photo` varchar(52) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `favorite`
--

CREATE TABLE `favorite` (
  `id` int(11) NOT NULL,
  `pseudodestinataire` varchar(52) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `idcompte` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `favorite`
--

INSERT INTO `favorite` (`id`, `pseudodestinataire`, `idcompte`) VALUES
(1, 'David', 5),
(2, 'Frank', 5),
(3, 'Omar', 5),
(4, 'Dino', 5),
(5, 'Frank', 3),
(6, 'Omar', 3),
(7, 'Dino', 3);

-- --------------------------------------------------------

--
-- Table structure for table `groupe`
--

CREATE TABLE `groupe` (
  `nomgroupe` varchar(52) NOT NULL,
  `dissipline` varchar(32) NOT NULL,
  `description` varchar(32) NOT NULL,
  `pseudocreateur` varchar(52) NOT NULL,
  `image` varchar(52) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `membre`
--

CREATE TABLE `membre` (
  `pseudo` varchar(52) NOT NULL,
  `prenom` varchar(52) NOT NULL,
  `nom` varchar(52) NOT NULL,
  `sexe` varchar(52) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `age` int(11) NOT NULL,
  `ville` varchar(52) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `pays` varchar(52) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `intention` varchar(52) NOT NULL,
  `photo` varchar(52) NOT NULL,
  `description` varchar(52) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `membre`
--

INSERT INTO `membre` (`pseudo`, `prenom`, `nom`, `sexe`, `age`, `ville`, `pays`, `intention`, `photo`, `description`) VALUES
('Claudiu', 'Claudiu', 'Tulba', 'Masculin', 43, 'Kirkland', 'Canada', 'amical', 'david.png', 'Velo'),
('David', 'David', 'Ramirez', 'Masculin', 33, 'Kirkland', 'Canada', 'amical', 'david.png', 'Velo'),
('Dino', 'Dino', 'Alegro', 'Masculin', 33, 'Kirkland', 'Canada', 'amical', 'dino.png', 'Velo'),
('Frank', 'Frank', 'Bienvenu', 'Masculin', 43, 'Kirkland', 'Canada', 'amical', 'david.png', 'Velo'),
('Omar', 'Omar', 'Aguilar', 'Masculin', 43, 'Kirkland', 'Canada', 'amical', 'david.png', 'Velo');

-- --------------------------------------------------------

--
-- Table structure for table `messagechat`
--

CREATE TABLE `messagechat` (
  `idmessage` int(11) NOT NULL,
  `pseudoorigin` varchar(52) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `message` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `idchat` int(11) NOT NULL,
  `datechat` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `messagechat`
--

INSERT INTO `messagechat` (`idmessage`, `pseudoorigin`, `message`, `idchat`, `datechat`) VALUES
(1, 'Claudiu', 'Bon matin', 1, '2021-02-13 13:36:56'),
(2, 'Claudiu', 'Hi David', 6, '2021-02-13 13:40:56'),
(3, 'Claudiu', 'Hello Frank', 7, '2021-02-13 13:41:58'),
(4, 'Dino', 'Hello Dino', 8, '2021-02-13 13:43:01'),
(5, 'Omar', 'Bon matin Claudiu', 9, '2021-02-13 13:43:52'),
(6, 'Omar', 'Hello David que est que tu a fait', 10, '2021-02-13 13:45:12'),
(7, 'Omar', 'Hello Frank je suis Omar', 8, '2021-02-13 13:45:51'),
(8, 'Omar', 'Hello Frank je suis omar', 7, '2021-02-13 13:45:51');

-- --------------------------------------------------------

--
-- Table structure for table `messagegroupe`
--

CREATE TABLE `messagegroupe` (
  `idmessage` int(11) NOT NULL,
  `nomgroupe` varchar(52) NOT NULL,
  `pseudoorigin` varchar(52) NOT NULL,
  `message` varchar(120) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `publicite`
--

CREATE TABLE `publicite` (
  `idpublicite` int(11) NOT NULL,
  `titre` varchar(52) NOT NULL,
  `image` varchar(52) NOT NULL,
  `description` varchar(120) NOT NULL,
  `idcompte` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`nomblog`),
  ADD KEY `pseudocreateur` (`pseudocreateur`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`idchat`),
  ADD KEY `pseudodestinataire` (`pseudodestinataire`),
  ADD KEY `pseudoorigin` (`pseudoorigin`);

--
-- Indexes for table `commentaireblog`
--
ALTER TABLE `commentaireblog`
  ADD KEY `pseudoorigin` (`pseudoorigin`),
  ADD KEY `nomblog` (`nomblog`);

--
-- Indexes for table `commentaireeventment`
--
ALTER TABLE `commentaireeventment`
  ADD KEY `pseudoorigin` (`pseudoorigin`),
  ADD KEY `nomevenement` (`nomevenement`);

--
-- Indexes for table `comptemembre`
--
ALTER TABLE `comptemembre`
  ADD PRIMARY KEY (`idcompte`),
  ADD UNIQUE KEY `courriel` (`courriel`),
  ADD KEY `pseudo` (`pseudo`);

--
-- Indexes for table `evenement`
--
ALTER TABLE `evenement`
  ADD PRIMARY KEY (`nomevenement`),
  ADD KEY `pseudocreateur` (`pseudocreateur`);

--
-- Indexes for table `favorite`
--
ALTER TABLE `favorite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pseudodestinataire` (`pseudodestinataire`),
  ADD KEY `idcompte` (`idcompte`);

--
-- Indexes for table `groupe`
--
ALTER TABLE `groupe`
  ADD PRIMARY KEY (`nomgroupe`),
  ADD KEY `pseudocreateur` (`pseudocreateur`);

--
-- Indexes for table `membre`
--
ALTER TABLE `membre`
  ADD PRIMARY KEY (`pseudo`);

--
-- Indexes for table `messagechat`
--
ALTER TABLE `messagechat`
  ADD PRIMARY KEY (`idmessage`),
  ADD KEY `pseudoorigin` (`pseudoorigin`),
  ADD KEY `idchat` (`idchat`);

--
-- Indexes for table `messagegroupe`
--
ALTER TABLE `messagegroupe`
  ADD PRIMARY KEY (`idmessage`),
  ADD KEY `pseudoorigin` (`pseudoorigin`),
  ADD KEY `nomgroupe` (`nomgroupe`);

--
-- Indexes for table `publicite`
--
ALTER TABLE `publicite`
  ADD PRIMARY KEY (`idpublicite`),
  ADD KEY `idcompte` (`idcompte`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `idchat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `comptemembre`
--
ALTER TABLE `comptemembre`
  MODIFY `idcompte` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `favorite`
--
ALTER TABLE `favorite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `messagechat`
--
ALTER TABLE `messagechat`
  MODIFY `idmessage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `messagegroupe`
--
ALTER TABLE `messagegroupe`
  MODIFY `idmessage` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `publicite`
--
ALTER TABLE `publicite`
  MODIFY `idpublicite` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog`
--
ALTER TABLE `blog`
  ADD CONSTRAINT `blog_ibfk_1` FOREIGN KEY (`pseudocreateur`) REFERENCES `membre` (`pseudo`);

--
-- Constraints for table `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `chat_ibfk_1` FOREIGN KEY (`pseudodestinataire`) REFERENCES `membre` (`pseudo`),
  ADD CONSTRAINT `chat_ibfk_2` FOREIGN KEY (`pseudoorigin`) REFERENCES `membre` (`pseudo`);

--
-- Constraints for table `commentaireblog`
--
ALTER TABLE `commentaireblog`
  ADD CONSTRAINT `commentaireblog_ibfk_1` FOREIGN KEY (`pseudoorigin`) REFERENCES `membre` (`pseudo`),
  ADD CONSTRAINT `commentaireblog_ibfk_2` FOREIGN KEY (`nomblog`) REFERENCES `blog` (`nomblog`);

--
-- Constraints for table `commentaireeventment`
--
ALTER TABLE `commentaireeventment`
  ADD CONSTRAINT `commentaireeventment_ibfk_1` FOREIGN KEY (`pseudoorigin`) REFERENCES `membre` (`pseudo`),
  ADD CONSTRAINT `commentaireeventment_ibfk_2` FOREIGN KEY (`nomevenement`) REFERENCES `evenement` (`nomevenement`);

--
-- Constraints for table `comptemembre`
--
ALTER TABLE `comptemembre`
  ADD CONSTRAINT `comptemembre_ibfk_1` FOREIGN KEY (`pseudo`) REFERENCES `membre` (`pseudo`);

--
-- Constraints for table `evenement`
--
ALTER TABLE `evenement`
  ADD CONSTRAINT `evenement_ibfk_1` FOREIGN KEY (`pseudocreateur`) REFERENCES `membre` (`pseudo`);

--
-- Constraints for table `favorite`
--
ALTER TABLE `favorite`
  ADD CONSTRAINT `favorite_ibfk_1` FOREIGN KEY (`pseudodestinataire`) REFERENCES `membre` (`pseudo`),
  ADD CONSTRAINT `favorite_ibfk_2` FOREIGN KEY (`idcompte`) REFERENCES `comptemembre` (`idcompte`);

--
-- Constraints for table `groupe`
--
ALTER TABLE `groupe`
  ADD CONSTRAINT `groupe_ibfk_1` FOREIGN KEY (`pseudocreateur`) REFERENCES `membre` (`pseudo`);

--
-- Constraints for table `messagechat`
--
ALTER TABLE `messagechat`
  ADD CONSTRAINT `messagechat_ibfk_1` FOREIGN KEY (`pseudoorigin`) REFERENCES `membre` (`pseudo`),
  ADD CONSTRAINT `messagechat_ibfk_2` FOREIGN KEY (`idchat`) REFERENCES `chat` (`idchat`);

--
-- Constraints for table `messagegroupe`
--
ALTER TABLE `messagegroupe`
  ADD CONSTRAINT `messagegroupe_ibfk_1` FOREIGN KEY (`pseudoorigin`) REFERENCES `membre` (`pseudo`),
  ADD CONSTRAINT `messagegroupe_ibfk_2` FOREIGN KEY (`nomgroupe`) REFERENCES `groupe` (`nomgroupe`);

--
-- Constraints for table `publicite`
--
ALTER TABLE `publicite`
  ADD CONSTRAINT `publicite_ibfk_1` FOREIGN KEY (`idcompte`) REFERENCES `comptemembre` (`idcompte`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
